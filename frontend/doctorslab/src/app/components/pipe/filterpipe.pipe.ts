import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterpipe'
})
export class FilterpipePipe implements PipeTransform {

  transform(testListRecord: any[], searchText: string): any[] {   
    if (!testListRecord) return [];
    if (!searchText) return testListRecord;
  
    return testListRecord.filter(item => {
      return Object.keys(item).some(key => {
        return String(item[key]).toLowerCase().includes(searchText.toLowerCase());
      });
    });
   }

}
