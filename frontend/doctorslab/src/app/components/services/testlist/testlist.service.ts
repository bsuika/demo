import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Testlab } from '../../models/testlab/testlab';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { retry, catchError } from 'rxjs/operators';
import { handleError } from 'src/app/helpers/exception-handling';

@Injectable({
  providedIn: 'root'
})
export class TestlistService {

  constructor(private http: HttpClient) {
  }

  /****************************************************************************
         Puropse: Api call for fetching list of all health packages in backend
         
*******************************************************************************/

  labListdata() : Observable<Testlab[]>{
   return this.http.get<Testlab[]>(`${environment.apiUrl}packages/getAllPackages`).pipe(
    retry(1),
    catchError(handleError)
  );
   }


}
