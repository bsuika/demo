import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/components/models/user/user';
import { Cart } from 'src/app/components/models/cart/cart';
import { AuthenticationService } from 'src/app/components/services/auth/authentication.service';
import { Router } from '@angular/router';
import { TestlistService } from 'src/app/components/services/testlist/testlist.service';
import { CartService } from 'src/app/components/services/cart/cart.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
/*******************Variables Decaraltion*************/
currentUser: User;
userId:number;
cartTotal: number = 0;
packages:Cart[];
  constructor(private authenticationService: AuthenticationService,private router: Router,
    private cartService:CartService,private testlistService:TestlistService) {
      //get the current user info 
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    const info=JSON.parse(localStorage.getItem('currentUser'));
    this.userId=info['user'].id; 
  }

  ngOnInit() {
    this.usercartList();
  }

  /************************************************************************
          Puropse: To Show the cart items list added of particular user in database
          Parameters: User Id

 **************************************************************************/
usercartList()
{
  
  this.cartService.cartListByUser(this.userId).subscribe(
   (cart:Cart[])=>{      
  
      this.packages = cart['list'];
      this.updateCartTotal();
    },err=>{
      console.error(err);
    }
    
  )
}

/************************************************************************
          Puropse: To update Total Value on changes of Qty of particlar Package/Test

 **************************************************************************/
updateCartTotal() {
  let total = 0;
  this.packages.map( elem => total = total + elem.testCount*elem.minPrice);
  this.cartTotal = total;
  }


}
