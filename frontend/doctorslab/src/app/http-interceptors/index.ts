import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptorsService } from './auth-interceptors.service';

export const httpInterceptProviders =[
    {
        provide : HTTP_INTERCEPTORS,useClass : AuthInterceptorsService, multi:true
    }
]